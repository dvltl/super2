#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>

#include "utils.h"

using namespace std;

//////////////////////////////////

vector<double> x;
vector<double> y;

const double eps = 0.0001;

//////////////////////////////////

// Should be used to get both Tau and Alpha 'cause the only difference is in params. Logic is similar.
// To get Alpha as @param first should be passed the result of full_neg_laplas()
double get_coeff(const vector< vector<double> > & first, const vector< vector<double> > & second) {
    vector< vector<double> > buf(first.size(), vector<double>(first[0].size(), 0));
    double top = get_euclid_mult(first, second, 1, first.size() - 1, 1, first[0].size() - 1, vector<double>(1,0));
    //cout << "dotres: " << top << endl;
    full_neg_laplas(second, buf, 1, first.size() - 1, 1, first[0].size() - 1, vector<double>(1,0), vector< vector<double> >(1, vector<double>(1,0)), 0);
    //print_vec(buf, "Full_neg_laplas in get_coeff:");
    double bot = get_euclid_mult(buf, second, 1, first.size() - 1, 1, first[0].size() - 1, vector<double>(1,0));
    return top / bot;
}

void init_border(vector< vector<double> >& p_0) {
    unsigned i_size = p_0.size(), j_size = p_0[0].size();
    int i = 0, j, k = i_size - 1, l;

    #pragma omp parallel for
    for (j = 0; j < j_size; ++j) {
        p_0[i][j] = border_func(x[i], y[j]);
        p_0[k][j] = border_func(x[k], y[j]);
    }

    j = 0; l = j_size - 1;
    #pragma omp parallel for
    for (i = 0; i < i_size; ++i) {
        p_0[i][j] = border_func(x[i], y[j]);
        p_0[i][l] = border_func(x[i], y[l]);
    }
}

double diff_between_matrs(const vector< vector<double> > & p_k, const vector< vector<double> > & p_k_1) {

    vector< vector<double> > diff = p_k_1;
    //print_vec(diff, "Diff before update");
    #pragma omp parallel for
    for (int i = 0; i < diff.size(); ++i) {
        for (int j = 0; j < diff[0].size(); ++j) {
            diff[i][j] -= p_k[i][j];
        }
    }
    //print_vec(diff, "Diff after update");
    return sqrt(get_euclid_mult(diff, diff, 1, diff.size() - 1, 1, diff[0].size() - 1, vector<double>(1,0)));
}

void iter0(vector< vector<double> > & p_k, vector< vector<double> > & r_k, vector< vector<double> > & g_k, const vector< vector<double> > & F) {
    init_border(p_k);
    get_r(p_k, r_k, F, 1, p_k.size() - 1, 1, p_k[0].size() - 1, vector<double>(1,0), vector< vector<double> >(1, vector<double>(1,0)), 0);
    g_k = r_k;
}

//////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc < 2) {
        cout << "Too few arguments" << endl;
        return -1;
    }

    double min_x = 0;
    double max_x = 2;
    double min_y = 0;
    double max_y = 2;
    double q = 1;

    int N = atoi(argv[1]);
    int x_parts = N;
    int y_parts = N;

    get_axis_lattice_coords(min_x, max_x, 0, x_parts, N, q, x);
    get_axis_lattice_coords(min_y, max_y, 0, y_parts, N, q, y);

    /*
    cout << "X Coords:" << endl;
    for (int i = 0; i < x_parts; ++i) {
        cout << x[i] << " ";
    }
    cout << endl;

    cout << "Y Coords:" << endl;
    for (int i = 0; i < y_parts; ++i) {
        cout << y[i] << " ";
    }
    cout << endl;
    */

    vector< vector<double> > p_k (x_parts, vector<double>(y_parts, 0));
    vector< vector<double> > prev;
    vector< vector<double> > r_k (x_parts, vector<double>(y_parts, 0));
    vector< vector<double> > g_k;
    vector< vector<double> > buf;

    vector< vector<double> > strict (x_parts, vector<double>(y_parts, 0));
    // pragma omp parallel
    for (int i = 0; i < x_parts; ++i) {
        for (int j = 0; j < y_parts; ++j) {
            strict[i][j] = border_func(x[i], y[j]);
        }
    }

    vector< vector<double> > F (x_parts, vector<double>(y_parts, 0));
    for (int i = 0; i < x_parts; ++i) {
        for (int j = 0; j < y_parts; ++j) {
            F[i][j] = lattice_func(x[i], y[j]);
        }
    }

    //print_vec(strict, "Strict solution values:");

    time_t start_time = time(NULL);

    iter0(p_k, r_k, g_k, F);

    int iter_num = 0;
    double alpha, tau, diff;

    do {
        cout << "Iter: " << ++iter_num << endl;
        prev = p_k;

        tau = get_coeff(r_k, g_k);
        cout << "Tau coeff: " << tau << endl;

        sub_with_coeff(p_k, g_k, p_k, tau);

        //print_2d_vec(p_k, "p_k_1:");

        get_r(p_k, r_k, F, 1, x_parts - 1, 1, y_parts - 1, vector<double>(1,0), vector< vector<double> >(1,vector<double>(1, 0)), 0);
        //print_2d_vec(r_k, "r_k_1");

        buf = vector< vector<double> >(x_parts, vector<double>(y_parts, 0));
        full_neg_laplas(r_k, buf, 1, x_parts - 1, 1, y_parts - 1, vector<double>(1,0), vector< vector<double> >(1,vector<double>(1, 0)), 0);
        //print_2d_vec(buf, "laplas_r_k_1");
        // before assignment g_k is actualy g_{k-1}
        //cout << "Getting alpha" << endl;
        alpha = get_coeff(buf, g_k);
        cout << "Alpha coeff: " << alpha << endl;
        buf.clear();

        // updating g_k
        sub_with_coeff(r_k, g_k, g_k, alpha);
        //print_2d_vec(g_k, "g_k_1:");

        diff = diff_between_matrs(prev, p_k);
        cout << "Difference: " << diff << "; epsilon: " << eps << endl;

        cout << "Diff between strict solution and mean solution: " << diff_between_matrs(strict, p_k) << endl;
    } while (diff > eps);

    time_t stop_time = time(NULL);
    /*
    diff = diff_between_matrs(strict, p_k);
    cout << "Diff between strict solution and mean solution: " << diff << endl;
    */
    cout << "All complete in " << iter_num << " iterations" << endl;
    cout << "Time used: " << difftime(stop_time, start_time) << " seconds" << endl;

    return 0;
}
