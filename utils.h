#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <iostream>
#include <fstream>
using namespace std;

extern vector<double> x;
extern vector<double> y;

void print_1d_vec(const int rank, const vector<double> & vec) {
    for (int i = 0; i < vec.size(); ++i) {
        cout << "Print_1d_vec for process " << rank << " " << vec[i] << endl;
    }
}

void print_2d_vec(const vector< vector<double> > & vec, const string & message = "", const int rank = 0) {
    if (!message.empty()) {
        cout << rank << " " << message << endl;
    }
    for (int i = 0; i < vec.size(); ++i) {
        for (int j = 0; j < vec[0].size(); ++j) {
            cout << "[" << i << "] [" << j << "] " << vec[i][j] << "; ";
        }
        cout << endl;
    }
}
/*
string to_string(const vector< vector<double> > & matr) {
    stringstream res;
    for (int i = 0; i < matr.size(); ++i) {
        for (int j = 0; j < matr[0].size(); ++j) {
            res << matr[i][j] << " ";
        }
        res << endl;
    }
}
*/
double point_neg_laplas(const double y_b, const double y_m, const double y_t,
                        const double y_bot_val, const double y_mid_val, const double y_top_val,
                        const double x_l, const double x_m, const double x_r,
                        const double x_lft_val, const double x_mid_val, const double x_rgt_val) {
    double h_i_mean_sw = 2.0f / (x_r - x_l);
    double x_part = h_i_mean_sw * ( ((x_mid_val - x_lft_val) / (x_m - x_l)) - ((x_rgt_val - x_mid_val) / (x_r - x_m)) );

    double h_j_mean_sw = 2.0f / (y_t - y_b);
    double y_part = h_j_mean_sw * ( ((y_mid_val - y_bot_val) / (y_m - y_b)) - ((y_top_val - y_mid_val) / (y_t - y_m)) );

    return x_part + y_part;
}

void full_neg_laplas(const vector< vector<double> > & p, vector< vector<double> > & result,
                     const int x_lb, const int x_gb, const int y_lb, const int y_gb,
                     const vector<double> & coord_shadow,
                     const vector< vector<double> > & shadow,
                     const int rank) {


    /*
    for (int i = 0; i < shadow.size(); ++i) {
        cout << rank << " shadow[" << i <<"] size: " << shadow[i].size() << endl;
        for (int j = 0; j < shadow[i].size(); ++j) {
            cout << rank << " " << i << " " << j << " " << shadow[i][j] << endl;
        }
    }
*/
    double y_lb_val, y_gb_val, x_lb_val, x_gb_val;
    double l_x, g_x, l_y, g_y;

    /*
    cout << rank << " x_lb: " << x_lb << " x_gb: " << x_gb << " y_lb: " << y_lb << " y_gb: " << y_gb
         << " " << (y_gb == (p[0].size() - 1)) << " " << (x_gb == (p.size() - 1)) << endl;
    print_2d_vec(shadow, "lapl shadow", rank);
    */

    #pragma omp parallel for
    for (int i = x_lb; i < x_gb; ++i) {
        l_x = x_lb == 1 ? x[i-1] : coord_shadow[2];
        l_x = i == x_lb ? l_x : x[i-1];
        g_x = x_gb == p.size() - 1 ? x[i+1] : coord_shadow[3];
        g_x = i == x_gb - 1 ? g_x : x[i+1];

        for (int j = y_lb; j < y_gb; ++j) {
            x_lb_val = x_lb == 1 ? p[i-1][j] : shadow[2][j];
            x_lb_val = i == x_lb ? x_lb_val : p[i-1][j];
            x_gb_val = x_gb == p.size() - 1 ? p[i+1][j] : shadow[3][j];
            x_gb_val = i == x_gb - 1 ? x_gb_val : p[i+1][j];

            l_y = y_lb == 1 ? y[j-1] : coord_shadow[0];
            l_y = j == y_lb ? l_y : y[j-1];
            g_y = y_gb == p[0].size() - 1 ? y[j+1] : coord_shadow[1];
            g_y = j == y_gb - 1 ? g_y : y[j+1];

            y_lb_val = y_lb == 1 ? p[i][j-1] : shadow[0][i];
            y_lb_val = j == y_lb ? y_lb_val : p[i][j-1];
            y_gb_val = y_gb == p[0].size() - 1 ? p[i][j+1] : shadow[1][i];
            y_gb_val = j == y_gb - 1 ? y_gb_val : p[i][j+1];

            /*
                cout << rank << " i: " << i << " l_x: " << l_x << " g_x: " << g_x;
                cout << " x_l_val: " << x_lb_val << " x_g_val: " << x_gb_val << endl;

                cout << rank << " j: " << j << " l_y: " << l_y << " g_y: " << g_y;
                cout << " y_l_val: " << y_lb_val << " y_g_val: " << y_gb_val << endl;
                cout << endl;
                */

            result[i][j] = point_neg_laplas(l_y, y[j], g_y, y_lb_val, p[i][j], y_gb_val,
                                            l_x, x[i], g_x, x_lb_val, p[i][j], x_gb_val);
        }
    }
    if (rank == 0) {
        //cout << "Call ended" << endl << endl;
    }
}

double get_euclid_mult(const vector< vector<double> > & u, const vector< vector<double> > & v,
                       const int x_l, const int x_g, const int y_l, const int y_g,
                       const vector<double> & coord_shadow) {
    double result = 0;
    double buf;
    double h_i_1, h_i, h_j_1, h_j;

    #pragma omp parallel for reduction(+:result)
    for (int i = x_l; i < x_g; ++i) {
        h_i_1 = x_l == 1 ? x[i] - x[i-1] : x[i] - coord_shadow[2];
        h_i_1 = i == x_l ? h_i_1 : x[i] - x[i-1];
        h_i = x_g == u.size() - 1 ? x[i + 1] - x[i] : coord_shadow[3] - x[i];
        h_i = i == x_g - 1 ? h_i : x[i+1] - x[i];

        buf = 0;
        for (int j = y_l; j < y_g; ++j) {
            h_j_1 = y_l == 1 ? y[j] - y[j-1] : y[j] - coord_shadow[0];
            h_j_1 = j == y_l ? h_j_1 : y[j] - y[j-1];
            h_j = y_g == u[0].size() - 1 ? y[j+1] - y[j] : coord_shadow[1] - y[j];
            h_j = j == y_g - 1 ? h_j : y[j+1] - y[j];

            buf += (h_j + h_j_1) * u[i][j] * v[i][j];
        }
        result += (h_i + h_i_1) * buf;
    }
    return (0.25 * result);
}

double f(double q, double t) {
    return (pow(1 + t, q) - 1) / (pow(2, q) - 1);
}

void get_axis_lattice_coords(const double min_border, const double max_border, const int start, const int end,
                             const int N, const double q, vector<double> & result) {
    const int true_end = (end > N) ? N : end;
    for (int i = start; i < true_end; ++i) {
        result.push_back(max_border * f(q, double(i) / (N - 1)) + min_border * (1 - f(q, double(i) / (N - 1))));
    }
}

double border_func(const double x, const double y) {
    return exp(1 - pow(x + y, 2));
}

double lattice_func(const double x, const double y) {
    return 4 * (1 - 2 * pow(x + y, 2)) * border_func(x, y);
}

void get_r(const vector< vector<double> > & p_k, vector< vector<double> > & r_k,
           const vector< vector<double> > & F,
           const int x_l, const int x_g, const int y_l, const int y_g,
           const vector<double> & coord_shadow, const vector< vector<double> > & shadow,
           const int rank) {
    //cout << rank << " begun laplas" << endl;
    full_neg_laplas(p_k, r_k, x_l, x_g, y_l, y_g, coord_shadow, shadow, rank);
    print_2d_vec(r_k, "laplas p_k_1", rank);
    //cout << rank << " passed laplas" << endl;
    #pragma omp parallel for
    for (int i = x_l; i < x_g; ++i) {
        for (int j = y_l; j < y_g; ++j) {
            r_k[i][j] -= F[i][j];
        }
    }
    /*
    cout << rank << " r_k:" << endl;
    print_2d_vec(r_k);
    */
}

void sub_with_coeff(const vector< vector<double> > & old_val,
                    const vector< vector<double> > & to_sub,
                    vector< vector<double> > & new_val,
                    const double coeff) {
    #pragma omp parallel for
    for (int i = 0; i < old_val.size(); ++i) {
        for (int j = 0; j < old_val[0].size(); ++j) {
            new_val[i][j] = old_val[i][j] - coeff * to_sub[i][j];
        }
    }
}

/*
inline void dump(const char * dir, const char * filename, const vector< vector<double> > & matr, const int rank, const int proc_num, const int N) {
    stringstream s;
    s << dir << "/" << filename << "_" << rank << "_" << proc_num << "_" << N << ".txt";
    fstream f;
    f.open(s.str().c_str(), fstream::out);
    f << to_string(matr);
    f.close();
}
*/
#endif // UTILS_H
