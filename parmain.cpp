#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <mpi.h>
#include <unistd.h>
//#include <omp.h>

#include "utils.h"

using namespace std;

//////////////////////////////////

vector<double> x;
vector<double> y;

const double eps = 0.0001;

//////////////////////////////////

void init_MPI_border(const vector< vector<double> > & p,
                     int border, const bool vertical,
                     vector<double> & result) {
    int i, j;
    result.clear();
    if (vertical) {
        border = border == p[0].size() ? border - 1 : border;
        for (i = 0; i < p.size(); ++i) {
            result.push_back(p[i][border]); // x=f(i), y=const - row
        }
    } else {
        border = border == p.size() ? border - 1 : border;
        for (j = 0; j < p[0].size(); ++j) {
            result.push_back(p[border][j]); // x=const, y=f(j) - column
        }
    }
}

void do_shadow_exchange(const int rank, const int y_r, const int x_r,
                 const int l_border, const int r_border,
                 const int u_border, const int d_border,
                 const vector< vector<double> > & p,
                 vector< vector<double> > & shadow) {
    vector<double> border_to_send;
    vector<double> border_to_recv (p.size(), 0);
    MPI_Status status;

    int x_size, y_size;
    x_size = p.size();
    y_size = p[0].size();

    // LEFT <-> proc <-> RIGHT
    //cout << "Process " << rank << " started exchange; x_size: " << x_size << "; y_size: " << y_size << endl;

    if (rank % 2 == 0) {
        // init border_to_send first
        init_MPI_border(p, r_border, true, border_to_send);
        //cout << rank << " inited right border" << endl;
        //print_1d_vec(rank, border_to_send);
        MPI_Send(&border_to_send[0], x_size, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD);
        MPI_Recv(&border_to_recv[0], x_size, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &status);
        shadow[1] = border_to_recv;
        if (rank % y_r != 0) {
            // init border_to_send first
            init_MPI_border(p, l_border, true, border_to_send);
            border_to_recv = vector<double>(x_size, 0);
            //cout << rank << " inited left border" << endl;
            //print_1d_vec(rank, border_to_send);
            MPI_Send(&border_to_send[0], x_size, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD);
            MPI_Recv(&border_to_recv[0], x_size, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &status);
            shadow[0] = border_to_recv;
        }
    } else {
        // init border_to_send first
        init_MPI_border(p, l_border, true, border_to_send);
        //cout << rank << " inited left border" << endl;
        //print_1d_vec(rank, border_to_send);
        MPI_Recv(&border_to_recv[0], x_size, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &status);
        MPI_Send(&border_to_send[0], x_size, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD);
        shadow[0] = border_to_recv;
        if (rank % y_r != y_r - 1) {
            // init border_to_send first
            init_MPI_border(p, r_border, true, border_to_send);
            border_to_recv = vector<double>(x_size, 0);
            //cout << rank << " inited right border" << endl;
            //print_1d_vec(rank, border_to_send);
            MPI_Recv(&border_to_recv[0], x_size, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &status);
            MPI_Send(&border_to_send[0], x_size, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD);
            shadow[1] = border_to_recv;
        }
    }

    // TOP <-> proc <-> BOTTOM
    border_to_recv.clear();
    border_to_recv = vector<double> (y_size, 0);

    if ((rank / y_r) % 2 == 0) {
        // init border_to_send first
        init_MPI_border(p, d_border, false, border_to_send);
        //cout << rank << " inited down border" << endl;
        //print_1d_vec(rank, border_to_send);
        if (rank / y_r != x_r - 1) {
            MPI_Send(&border_to_send[0], y_size, MPI_DOUBLE, rank + y_r, 0, MPI_COMM_WORLD);
            MPI_Recv(&border_to_recv[0], y_size, MPI_DOUBLE, rank + y_r, 0, MPI_COMM_WORLD, &status);
            shadow[3] = border_to_recv;
        }
        if (rank / y_r != 0) {
            // init border_to_send first
            init_MPI_border(p, u_border, false, border_to_send);
            border_to_recv = vector<double>(y_size, 0);
            //cout << rank << " inited up border" << endl;
            //print_1d_vec(rank, border_to_send);
            MPI_Send(&border_to_send[0], y_size, MPI_DOUBLE, rank - y_r, 0, MPI_COMM_WORLD);
            MPI_Recv(&border_to_recv[0], y_size, MPI_DOUBLE, rank - y_r, 0, MPI_COMM_WORLD, &status);
            shadow[2] = border_to_recv;
        }
    } else {
        // init border_to_send first
        init_MPI_border(p, u_border, false, border_to_send);
        //cout << rank << " inited up border" << endl;
        //print_1d_vec(rank, border_to_send);
        if (rank / y_r != 0) {
            MPI_Recv(&border_to_recv[0], y_size, MPI_DOUBLE, rank - y_r, 0, MPI_COMM_WORLD, &status);
            MPI_Send(&border_to_send[0], y_size, MPI_DOUBLE, rank - y_r, 0, MPI_COMM_WORLD);
            shadow[2] = border_to_recv;
        }
        if (rank / y_r != x_r - 1) {
            // init border_to_send first
            init_MPI_border(p, d_border, false, border_to_send);
            border_to_recv = vector<double>(y_size, 0);
            //cout << rank << " inited down border" << endl;
            //print_1d_vec(rank, border_to_send);
            MPI_Recv(&border_to_recv[0], y_size, MPI_DOUBLE, rank + y_r, 0, MPI_COMM_WORLD, &status);
            MPI_Send(&border_to_send[0], y_size, MPI_DOUBLE, rank + y_r, 0, MPI_COMM_WORLD);
            shadow[3] = border_to_recv;
        }
    }

    //cout << "Process " << rank << " finished exchange" << endl;

    /*
    usleep(rank * 1000);
    cout << rank << endl;
    print_2d_vec(shadow, "Shadow");
*/

    MPI_Barrier(MPI_COMM_WORLD);
}

void MPI_get_coeff(const vector< vector<double> > & first, const vector< vector<double> > & second,
                   vector<double> & res,
                   const int x_lb, const int x_gb, const int y_lb, const int y_gb,
                   const vector< vector<double> > & shadow, const vector<double> & coord_shadow,
                   const int rank) {
    vector <double> tb (2, 0);
    tb[0] = get_euclid_mult(first, second, x_lb, x_gb, y_lb, y_gb, coord_shadow);
    vector< vector<double> > buf = vector< vector<double> >(second.size(), vector<double>(second[0].size(), 0));

    full_neg_laplas(second, buf, x_lb, x_gb, y_lb, y_gb, coord_shadow, shadow, rank);
    tb[1] = get_euclid_mult(buf, second, x_lb, x_gb, y_lb, y_gb, coord_shadow);

    res[0] = res[1] = 0;

    MPI_Allreduce(&tb[0], &res[0], 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
}

double MPI_diff_between_matrs(const vector< vector<double> > & u, const vector< vector<double> > & v,
                            const int x_l, const int x_g, const int y_l, const int y_g,
                            const vector<double> & coord_shadow) {
    vector< vector<double> > diff = u;
    for (int i = x_l; i < x_g; ++i) {
        for (int j = y_l; j < y_g; ++j) {
            diff[i][j] -= v[i][j];
        }
    }
    double local_eucl = get_euclid_mult(diff, diff, x_l, x_g, y_l, y_g, coord_shadow);
    double global_eucl;
    MPI_Allreduce(&local_eucl, &global_eucl, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    return sqrt(global_eucl);
}

//////////////////////////////////

void iter0(vector< vector<double> > & p_k, vector< vector<double> > & r_k,
           vector< vector<double> > & g_k, const vector< vector<double> > & F,
           const int rank, const int y_r, const int x_r,
           const int y_l, const int y_g,
           const int x_l, const int x_g,
           const vector<double> & coord_shadow,
           vector< vector<double> > & shadow) {

    if (x_l == 1) {
        for (int j = 0; j < p_k[0].size(); ++j) {
            p_k[x_l - 1][j] = border_func(x[x_l - 1], y[j]);
        }
    }
    if (x_g == p_k.size() - 1) {
        for (int j = 0; j < p_k[0].size(); ++j) {
            p_k[x_g][j] = border_func(x[x_g], y[j]);
        }
    }

    if (y_l == 1) {
        for (int i = 0; i < p_k.size(); ++i) {
            p_k[i][y_l - 1] = border_func(x[i], y[y_l - 1]);
        }
    }
    if (y_g == p_k[0].size() - 1) {
        for (int i = 0; i < p_k.size(); ++i) {
            p_k[i][y_g] = border_func(x[i], y[y_g]);
        }
    }

    do_shadow_exchange(rank, y_r, x_r, y_l, y_g, x_l, x_g, p_k, shadow);

    get_r(p_k, r_k, F, x_l, x_g, y_l, y_g, coord_shadow, shadow, rank);
    g_k = r_k;
}

//////////////////////////////////

int main(int argc, char *argv[])
{
    if (argc < 2) {
        cout << "Too few arguments" << endl;
        cout << "Expected: 2, got: " << argc << endl;
        for (int i = 0; i < argc; ++i) {
            cout << "Argv[" << i << "]: " << argv[i] << endl;
        }
        return -1;
    }

    double min_x = 0;
    double max_x = 2;
    double min_y = 0;
    double max_y = 2;
    double q = 1;

    // initialization of MPI services
    MPI_Init(&argc, &argv);

    int rank, proc_count;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_count);

    int N = atoi(argv[1]);

    int x_parts = N;
    int y_parts = N;

    int y_r, x_r, x_lb, x_gb, y_lb, y_gb;

    y_r = log(sqrt(proc_count)) / log(2);
    x_r = (int)(log(proc_count) / log(2)) - y_r;

    y_r = (int) pow(2, (double) y_r); // processors per row
    x_r = (int) pow(2, (double) x_r); // processors per column

    if (x_parts % x_r != 0) {
        if (rank / x_r == x_r - 1) {
            x_parts = x_parts - ((x_r - 1) * (x_parts + x_r - 1) / x_r);
        } else {
            x_parts = (x_parts + x_r - 1) / x_r;
        }
    } else {
        x_parts /= x_r;
    }

    if (y_parts % y_r != 0) {
        if (rank % y_r == y_r - 1) {
            y_parts = y_parts - ((y_r - 1) * (y_parts + y_r - 1) / y_r);
        } else {
            y_parts = (y_parts + y_r - 1) / y_r;
        }
    } else {
        y_parts /= y_r;
    }

    //cout << "Rank deps: " << rank << " " << rank / y_r << " " << x_r - 1 << " y_parts: " << y_parts << " y_r: " << y_r << " %y_r: " << rank % y_r << endl;

    if (rank / y_r == 0) {
        x_lb = 1; x_gb = x_parts;
    } else if (rank / y_r == x_r - 1) {
        x_lb = 0; x_gb = x_parts - 1;
    } else {
        x_lb = 0; x_gb = x_parts;
    }

    if (y_r != 1) {
        if (rank % y_r == 0) {
            y_lb = 1; y_gb = y_parts;
        } else if (rank % y_r == y_r - 1) {
            y_lb = 0; y_gb = y_parts - 1;
        } else {
            y_lb = 0; y_gb = y_parts;
        }
    } else {
        y_lb = 1; y_gb = y_parts - 1;
    }

    int y_end = (rank + 1) % y_r == 0 ? N : ((rank + 1) % y_r) * y_parts;

    get_axis_lattice_coords(min_x, max_x, (rank / y_r) * x_parts, (rank / y_r + 1) * x_parts, N, q, x);
    get_axis_lattice_coords(min_y, max_y, (rank % y_r) * y_parts, y_end, N, q, y);

    cout << rank << " X_l: " << x_lb << " X_g: " << x_gb << endl;
    cout << rank << " Y_l: " << y_lb << " Y_g: " << y_gb << endl;
    cout << rank << " " << y_r << " " << y_parts << endl;
    cout << rank << " " << (rank % y_r) * y_parts << " " << ((rank + 1) % y_r) * y_parts << endl;

    vector< vector<double> > p_k (x_parts, vector<double>(y_parts, 0));
    vector< vector<double> > r_k (x_parts, vector<double>(y_parts, 0));
    vector< vector<double> > g_k;
    vector< vector<double> > buf;
    vector< vector<double> > prev;

    vector< vector<double> > strict (x_parts, vector<double>(y_parts, 0));
    // pragma omp parallel
    for (int i = 0; i < x_parts; ++i) {
        for (int j = 0; j < y_parts; ++j) {
            strict[i][j] = border_func(x[i], y[j]);
        }
    }

    vector< vector<double> > F (x_parts, vector<double>(y_parts, 0));
    for (int i = 0; i < x_parts; ++i) {
        for (int j = 0; j < y_parts; ++j) {
            F[i][j] = lattice_func(x[i], y[j]);
        }
    }

    //print_vec(strict, "Strict solution values:");
    //print_vec(F, "Lattice function values:");

    time_t start_time = time(NULL);

    // cout << "Process " << rank << " is right before coord_shadow init" << endl;

    vector<double> coord_shadow(4, 0);
    if (rank % y_r != 0) {
        double arg = (double)((rank % y_r) * y_parts - 1)/ (N - 1);
        coord_shadow[0] = max_y * f(q, arg) + min_y * (1 - f(q, arg)); // y[j-1]
        cout << rank << " " << arg << " i:0 c: " << coord_shadow[0] << endl;
    }
    if (rank % y_r != y_r - 1) {
        double arg = (double)(((rank + 1)  % y_r) * y_parts)/ (N - 1);
        coord_shadow[1] = max_y * f(q, arg) + min_y * (1 - f(q, arg)); // y[j+1]
        cout << rank << " " << arg << " i:1 c: " << coord_shadow[1] << endl;
    }
    if (rank / y_r != 0) {
        double arg = (double)((rank / y_r) * x_parts - 1)/ (N - 1);
        coord_shadow[2] = max_x * f(q, arg) + min_x * (1 - f(q, arg)); // x[i-1]
        cout << rank << " " << arg << " i:2 c: " << coord_shadow[2] << endl;
    }
    if (rank / y_r != x_r - 1) {
        double arg = (double)((rank / y_r + 1) * x_parts)/ (N - 1);
        coord_shadow[3] = max_x * f(q, arg) + min_x * (1 - f(q, arg)); // x[i+1]
        cout << rank << " " << arg << " i:3 c: " << coord_shadow[3] << endl;
    }

    /*
    cout << rank << " y" << endl;
    //print_1d_vec(rank, y);

    cout << rank << " x" << endl;
    //print_1d_vec(rank, x);
*/

    int iter_num = 0;
    double alpha, tau, diff;
    vector< vector<double> > shadow (4, vector<double>(p_k[0].size(),0));
    vector< vector<double> > g_shadow = shadow;
    vector< vector<double> > r_shadow = shadow;
    vector<double> res (2, 0);

    cout << "Process " << rank << " finished with init" << endl;

    iter0(p_k, r_k, g_k, F, rank, y_r, x_r, y_lb, y_gb, x_lb, x_gb, coord_shadow, shadow);

    /*
    usleep(rank * 1000);
    print_2d_vec(p_k, "p_k", rank);
*/
    usleep(rank * 1000);
    print_2d_vec(r_k, "r_k", rank);


    do {
        if (rank == 0) {
            cout << rank << " Iter: " << ++iter_num << endl;
        }
        prev = p_k;

        do_shadow_exchange(rank, y_r, x_r, y_lb, y_gb, x_lb, x_gb, g_k, g_shadow);

        MPI_get_coeff(r_k, g_k, res, x_lb, x_gb, y_lb, y_gb, g_shadow, coord_shadow, rank);
        tau = res[0] / res[1];

        if (rank == 0) {
            cout << "Tau: " << tau << " top: " << res[0] << " bot: " << res[1] << endl;
        }

        sub_with_coeff(p_k, g_k, p_k, tau);

        usleep(rank * 1000);
        print_2d_vec(p_k, "p_k_1", rank);

        do_shadow_exchange(rank, y_r, x_r, y_lb, y_gb, x_lb, x_gb, p_k, shadow);

        print_2d_vec(shadow, "shadow", rank);
        MPI_Barrier(MPI_COMM_WORLD);

        get_r(p_k, r_k, F, x_lb, x_gb, y_lb, y_gb, coord_shadow, shadow, rank);

        usleep(rank * 1000);
        print_2d_vec(r_k, "r_k_1", rank);

        buf = vector< vector<double> >(x_parts, vector<double>(y_parts, 0));

        do_shadow_exchange(rank, y_r, x_r, y_lb, y_gb, x_lb, x_gb, r_k, r_shadow);
        full_neg_laplas(r_k, buf, x_lb, x_gb, y_lb, y_gb, coord_shadow, r_shadow, rank);

        MPI_get_coeff(buf, g_k, res, x_lb, x_gb, y_lb, y_gb, g_shadow, coord_shadow, rank);
        alpha = res[0] / res[1];
        buf.clear();

        if (rank == 0) {
            cout << "Alpha: " << alpha << endl;
        }

        // updating g_k
        sub_with_coeff(r_k, g_k, g_k, alpha);

        diff = MPI_diff_between_matrs(prev, p_k, x_lb, x_gb, y_lb, y_gb, coord_shadow);
        //cout << rank << " Difference: " << diff << "; epsilon: " << eps << endl;

        double strict_diff = MPI_diff_between_matrs(strict, p_k, x_lb, x_gb, y_lb, y_gb, coord_shadow);
        if (rank == 0) {
            cout << "Diff between strict solution and mean solution: " << strict_diff << endl;
        }

    } while (diff > eps);

    time_t stop_time = time(NULL);
    //diff = diff_between_iters(strict, p_k);
    //cout << "Diff between strict solution and mean solution: " << diff << endl;
    if (rank == 0) {
        cout << "All complete in " << iter_num << " iterations" << endl;
        cout << "Time used: " << difftime(stop_time, start_time) << " seconds" << endl;
    }
    return 0;
}
